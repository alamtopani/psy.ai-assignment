# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

**Please Install**

* Ruby version 2.7.1p83

* Rails version 7.0.8.1

* Database PostgreSQL

* Install Redis


**How to Run**

* git clone git@gitlab.com:alamtopani/psy.ai-assignment.git

* bundle install

* bundle exec rake db:create

* bundle exec rake db:migrate

* rails s

* in other tab terminal run redis-server 

* in other tab terminal run bundle exec sidekiq



