class UsersController < ApplicationController
  def index
    @users = ListUser.new(params).call
  end

  def destroy
    @user = User.find(params[:id])
    if @user.destroy
      CountUserDaily.new(today: @user.created_at).call
      UpdateUserCount.new(time: @user.created_at).call
      redirect_to root_path, notice: 'User was successfully deleted.'
    else
      redirect_to root_path, alert: 'User fails to delete.'
    end
  end
end