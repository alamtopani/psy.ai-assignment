class User < ApplicationRecord
  self.primary_key = 'id'

  MALE = 'male'
  FEMALE = 'female'
end
