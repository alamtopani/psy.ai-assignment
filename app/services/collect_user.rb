require 'faraday'

class CollectUser
  def initialize(number)
    @number = number
    @client = connection("https://randomuser.me")
  end

  def connection(url)
    Faraday.new(url: url) do |faraday|
      faraday.ssl.verify = false
      faraday.request  :url_encoded
      faraday.response :logger
      faraday.adapter  Faraday.default_adapter # Net::HTTP
    end
  end

  def call    
    response = @client.get("/api/?results=#{@number}")
    raise ::StandardError, 'Erros get users' if response.status != 200

    response_body = JSON.parse(response.body)
    response_body['results'].each do |user|
      us = User.find_or_initialize_by(uuid: user['login']['uuid'])
      us.gender = user['gender']
      us.age = user['dob']['age']
      us.name = user['name']
      us.location = user['location']
      us.save
    end
    UpdateUserCount.new.call
    true
  rescue StandardError => e
    raise ::StandardError, e
  end
end