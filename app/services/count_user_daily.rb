class CountUserDaily
  def initialize(today: nil)
    @today = today || Time.now.utc
  end

  def call
    redis_hourly_male_count = 0
    redis_hourly_female_count = 0
    calculate_daily = (0..24).each do |hour|
      key = "hourly_value_#{hour}_#{@today.day}"
      get_data = $redis.get(key)
      if get_data.present?
        d = JSON.parse(get_data) 
        redis_hourly_male_count += d['male']
        redis_hourly_female_count += d['female']
      end
    end

    us = DailyRecord.find_or_initialize_by(date: @today)
    us.male_count = redis_hourly_male_count > 0 ? redis_hourly_male_count : user_count(User::MALE)
    us.female_count = redis_hourly_female_count > 0 ? redis_hourly_female_count : user_count(User::FEMALE)
    us.male_avg_age = count[:male_avg_age]
    us.female_avg_age = count[:female_avg_age]
    us.save
  rescue StandardError => e
    raise ::StandardError, e
  end

  private

  def count
    @data = {
      male_avg_age: user_avg(User::MALE),
      female_avg_age: user_avg(User::FEMALE)
    }
  end

  def collection_user(gender)
    start_of_day = @today.beginning_of_day
    end_of_day = @today.end_of_day

    User.where(gender: gender).where(created_at: start_of_day..end_of_day)
  end

  def user_count(gender)
    @count = collection_user(gender).count
  end

  def user_avg(gender)
    @count = collection_user(gender).average(:age)
  end
end