class ListReport
  def initialize(params)
    @params = params
  end

  def call
    reports = DailyRecord.order(date: :desc)

    if @params[:start_date].present?
      reports = reports.where("DATE(daily_records.date) >=?", @params[:start_date].to_date)
    end

    if @params[:end_date].present?
      reports = reports.where("DATE(daily_records.date) <=?", @params[:end_date].to_date)
    end

    reports
  end
end