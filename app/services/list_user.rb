class ListUser
  def initialize(params)
    @params = params
  end

  def call
    users = User.order(created_at: :desc)

    if @params[:keywords].present?
      query_opts = [
        "users.name ->> 'first' ILIKE LOWER(:key)", 
        "users.name ->> 'last' ILIKE LOWER(:key)", 
        "LOWER(users.uuid) LIKE LOWER(:key)"
      ].join(' OR ')
      users = users.where(query_opts, { key: "%#{@params[:keywords]}%" })
    end

    if @params[:location].present?
      query_opts = [
        "users.location ->> 'country' ILIKE LOWER(:key)", 
        "users.location ->> 'city' ILIKE LOWER(:key)"
      ].join(' OR ')
      users = users.where(query_opts, { key: "%#{@params[:location]}%" })
    end

    if @params[:gender].present?
      users = users.where(gender: @params[:gender])
    end

    users
  end
end