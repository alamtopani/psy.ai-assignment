class UpdateUserCount
  def initialize(time: nil)
    @time = time || Time.now.utc
    @one_hour_ago = @time - 1.hour
    @key = "hourly_value_#{@time.hour}_#{@time.day}"
    @expiration_in_seconds = 24 * 60 * 60  # 1 day in seconds
  end

  def call
    $redis.set(@key, count.to_json, ex: @expiration_in_seconds)
  end

  private

  def count
    @data = {
      male: user_count(User::MALE),
      female: user_count(User::FEMALE)
    }
  end

  def user_count(gender)
    @count = User.where(gender: gender).where(created_at: @one_hour_ago..@time).count
  end
end