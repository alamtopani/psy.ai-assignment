threads_count = ENV.fetch("RAILS_MAX_THREADS") { 5 }.to_i
threads threads_count, threads_count

port        ENV.fetch("PORT") { 3000 }
plugin :tmp_restart


if ENV['RAILS_ENV'] == 'staging'
  ########### FOR DEPLOY 
  # Dir
  shared_dir = '/home/psy_backend/psy_backend'

  # Set up socket location
  bind "unix://#{shared_dir}/shared/tmp/sockets/puma.sock"

  # Logging
  stdout_redirect "#{shared_dir}/shared/log/puma.stdout.log", "#{shared_dir}/shared/log/puma.stderr.log", false

  # Set master PID and state locations
  pidfile "#{shared_dir}/shared/tmp/pids/puma.pid"
  state_path "#{shared_dir}/shared/tmp/pids/puma.state"
  activate_control_app 'unix://#{shared_dir}/shared/tmp/sockets/pumactl.sock'

  workers 2
  threads 1,2

  daemonize true

  prune_bundler
end