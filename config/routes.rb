Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "reports#index"
  get 'reports', to: 'reports#index', as: 'reports'
  get 'users', to: 'users#index', as: 'users'
  delete 'users/:id', to: 'users#destroy', as: 'delete_user'
end
