set :environment, "staging"
set :output, "/home/psy_backend/psy_backend/shared/log/cron_log.log"
set :bundle_command, "/home/psy_backend/.rbenv/shims/bundle exec"

every '0 * * * *' do
  runner "CollectUser.new(20).call"
end

every '0 0 * * *' do
  runner "CountUserDaily.new.call"
end