class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users do |t|
      t.string :uuid
      t.jsonb :name, default: {}
      t.jsonb :location, default: {}
      t.string :gender
      t.integer :age

      t.timestamps
    end

    add_index :users, :uuid
  end
end
